# Microseen Chat (BackEnd)

## Project setup
```
npm install
```

### Compiles for production
```
npm run build
```

### Compiles and runs for development
```
npm run dev
```

### Runs for production
```
npm run prod
```

### Configuration
```
Create a file named .env.local in the root directory of the project.
Use the file named .env as an exmaple to fill .env.local file
```
