import { NextFunction, Request } from 'express';
import * as WebSocket from 'ws';

export default class WSRouter {
  public readonly ws: WebSocket;

  public readonly req: Request;

  public readonly next: NextFunction;

  public middleWares: {
    message: string,
    callback: (ws: WebSocket, req: Request, next: NextFunction) => any,
  }[] = [];

  constructor(ws: WebSocket, req: Request, next: NextFunction) {
    this.ws = ws;
    this.req = req;
    this.next = next;
    this.start();
  }

  public start() {
    this.ws.onmessage = (event: WebSocket.MessageEvent) => {
      try {
        const data = JSON.parse(event.data.toString());
        const middleWare = this.middleWares.find(m => m.message === data.method);

        if (middleWare) {
          middleWare.callback(this.ws, this.req, this.next);
        }
        // eslint-disable-next-line no-empty
      } catch (e) { }
    };
  }

  public route(
    message: string,
    callback: (ws: WebSocket, req: Request, next: NextFunction) => any,
  ) {
    this.middleWares.push({
      message,
      callback,
    });
  }
}
