import * as oauth2orize from 'oauth2orize';
import passport from 'passport';
import { Connection, getConnection } from 'typeorm';
import AccessToken from '../models/AccessToken';
import User from '../models/User';
import Auth from './Auth';

// create OAuth 2.0 server
const server = oauth2orize.createServer();

// Exchange username & password for access token.
server.exchange(oauth2orize.exchange.password(
  async (client, username, password, scope, done) => {
    const connection: Connection = getConnection();
    try {
      const user = await connection.getRepository(User)
        .findOne({
          where: [
            { username },
            { email: username },
          ],
        });

      if (!user) return done(null, false);
      if (!Auth.checkSaltHash(password, user.salt, user.hash)) return done(null, false);

      let accessToken = connection.getRepository(AccessToken).create({
        token: await Auth.createToken(),
        client_id: client.id,
        user_id: user.id,
        created_at: Date.now(),
      });
      accessToken = await connection.getRepository(AccessToken).save(accessToken);
      return done(null, accessToken.token, false,
        { expires_in: accessToken.expires_in, user_id: user.id });
    } catch (e) {
      return done(e);
    }
  },
));

// token endpoint
export default [
  passport.authenticate(['basic', 'oauth2-client-password'], { session: false }),
  server.token(),
  server.errorHandler(),
];
