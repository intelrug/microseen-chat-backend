import express from 'express';
import passport from 'passport';
import bodyParser from 'body-parser';
import { createConnection } from 'typeorm';
import expressWs, { Application } from 'express-ws';
import Routes from './routes/Routes';
import Auth from './lib/Auth';

class App {
  public app: Application;

  constructor() {
    this.config();
    Routes.routes(this.app);
    App.setupDb();
  }

  private config(): void {
    const app = express();
    const wsInstance = expressWs(app);
    this.app = wsInstance.app;
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(express.static('static'));
    this.app.use(passport.initialize());
    Auth.init();
  }

  private static async setupDb() {
    await createConnection({
      type: 'mysql',
      host: process.env.DB_HOST,
      port: parseInt(process.env.DB_PORT, 10),
      username: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DB_NAME,
      synchronize: true,
      logging: false,
      entities: [
        `${__dirname}/models/**/*`,
      ],
    });
  }
}

export default new App().app;
