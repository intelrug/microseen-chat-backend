import { Application } from 'express-ws';
import passport from 'passport';
import WSRouter from '../lib/WSRouter';
import Message from '../controllers/Message';
import OAuth2 from '../lib/OAuth2';
import User from '../controllers/User';

export default class Routes {
  public static routes(app: Application) {
    app.route(/^\/(?!unsecured).*$/).all(passport.authenticate('bearer', { session: false }));
    app.route('/unsecured/oauth/token').post(OAuth2);
    app.route('/unsecured/register').post(User.create);

    app.ws('/', (ws, req, next) => {
      const router: WSRouter = new WSRouter(ws, req, next);

      router.route('get_message', Message.getOne);
      router.route('get_messages', Message.getMany);
      router.route('send_message', Message.send);
      router.route('delete_message', Message.delete);
      router.route('edit_message', Message.edit);
    });
  }
}
