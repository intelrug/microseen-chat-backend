import {
  Column, Entity, JoinColumn, ManyToOne, ObjectType, PrimaryGeneratedColumn,
} from 'typeorm';
import User from './User';

@Entity('messages')
export default class Message {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public chat_id: number;

  @Column()
  public author_id: number;

  @Column('text')
  public content: string;

  @Column('bigint')
  public created_at: number;

  @Column('bigint')
  public updated_at: number;

  @Column('tinyint')
  public is_deleted: number;

  @ManyToOne((): ObjectType<User> => User, (user): Message[] => user.messages)
  @JoinColumn({ name: 'author_id' })
  public author: User;
}
