import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('access_tokens')
export default class AccessToken {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  user_id: number;

  @Column()
  client_id: number;

  @Column({
    unique: true,
  })
  token: string;

  @Column('bigint')
  created_at: number;

  @Column({
    type: 'bigint',
    default: 0,
  })
  expires_in: number;

  @Column({
    type: 'bigint',
    default: 0,
  })
  active_at: number;
}
