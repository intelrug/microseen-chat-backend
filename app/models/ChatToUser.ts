import {
  Column, Entity, JoinColumn, ManyToOne, ObjectType, PrimaryGeneratedColumn,
} from 'typeorm';
import Chat from './Chat';
import User from './User';

@Entity('chat_to_user')
export default class ChatToUser {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public chat_id: number;

  @Column()
  public user_id: number;

  @Column('tinyint')
  public is_joined: number;

  @Column('tinyint')
  public is_banned: number;

  @Column('bigint')
  public active_at: number;

  @Column('bigint')
  public typed_at: number;

  @ManyToOne((): ObjectType<Chat> => Chat, (chat): ChatToUser[] => chat.chat_to_users, {
    primary: true,
  })
  @JoinColumn({
    name: 'chat_id',
  })
  public chat: Chat;

  @ManyToOne((): ObjectType<User> => User, (user): ChatToUser[] => user.chat_to_users, {
    primary: true,
  })
  @JoinColumn({
    name: 'user_id',
  })
  public user: User;
}
