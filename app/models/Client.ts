import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('clients')
export default class Client {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  secret: string;
}
