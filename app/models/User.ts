import {
  Column, Connection, Entity, getConnection, ObjectType, OneToMany, PrimaryGeneratedColumn,
} from 'typeorm';
import ChatToUser from './ChatToUser';
import Chat from './Chat';
import Message from './Message';
import APICode from '../lib/APICode';
import Auth from '../lib/Auth';

@Entity('users')
export default class User {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public username: string;

  @Column()
  public hash: string;

  @Column()
  public salt: string;

  @Column()
  public email: string;

  @Column('bigint')
  public registered_at: number;

  @Column({
    type: 'bigint',
    default: 0,
  })
  public active_at: number;

  @OneToMany((): ObjectType<ChatToUser> => ChatToUser, (chatToUser): User => chatToUser.user)
  public chat_to_users: ChatToUser[];

  @OneToMany((): ObjectType<Chat> => Chat, (chat): User => chat.creator)
  public chats: Chat[];

  @OneToMany((): ObjectType<Message> => Message, (message): User => message.author)
  public messages: Message[];

  public static async getByPk(userId: number): Promise<User> {
    const user: User = await getConnection().getRepository(User).findOne(userId, {
      select: [
        'id',
        'username',
        'email',
        'active_at',
        'registered_at',
      ],
    });
    if (!user) throw new APICode(2);
    return user;
  }

  public static async register(username: string, email: string, password: string): Promise<User> {
    const connection: Connection = getConnection();
    let user: User = await connection.getRepository(User)
      .findOne({
        where: [
          { username },
          { email: username },
        ],
      });

    if (user) {
      if (user.username === username) throw new APICode(300);
      if (user.email === email) throw new APICode(301);
    }

    const { salt, hash } = await Auth.createSaltHash(password);

    user = await connection.getRepository(User).create({
      username,
      salt,
      hash,
      email,
      registered_at: Date.now(),
    });
    user = await connection.getRepository(User).save(user);

    return user;
  }
}
