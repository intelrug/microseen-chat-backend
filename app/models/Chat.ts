import {
  Column, Entity, JoinColumn, ManyToOne, ObjectType, OneToMany, PrimaryGeneratedColumn,
} from 'typeorm';
import ChatToUser from './ChatToUser';
import User from './User';

@Entity('chats')
export default class Chat {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public name: string;

  @Column()
  public description: string;

  @Column('bigint')
  public created_at: number;

  @Column()
  public creator_id: number;

  @Column('tinyint')
  public is_deleted: number;

  @OneToMany((): ObjectType<ChatToUser> => ChatToUser, (chatToUser): Chat => chatToUser.chat)
  public chat_to_users: ChatToUser[];

  @ManyToOne((): ObjectType<User> => User, (user): Chat[] => user.chats)
  @JoinColumn({ name: 'creator_id' })
  public creator: User;
}
