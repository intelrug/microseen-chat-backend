import { NextFunction, Request } from 'express';
import * as WebSocket from 'ws';

export default class MessageC {
  public static getOne(ws: WebSocket, req: Request, next: NextFunction) {
    console.log('getOne');
  }

  public static getMany(ws: WebSocket, req: Request, next: NextFunction) {
    console.log('getMany');
  }

  public static send(ws: WebSocket, req: Request, next: NextFunction) {
    console.log('send');
  }

  public static delete(ws: WebSocket, req: Request, next: NextFunction) {
    console.log('delete');
  }

  public static edit(ws: WebSocket, req: Request, next: NextFunction) {
    console.log('edit');
  }
}
