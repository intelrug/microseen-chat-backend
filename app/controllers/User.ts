import { Request, Response } from 'express';
import Helper from '../lib/Helper';
import User from '../models/User';

export default class UserC {
  public static async create(req: Request, res: Response) {
    const { username } = req.body;
    const { email } = req.body;
    const { password } = req.body;
    try {
      let user: User = await User.register(username, email, password);
      user = await User.getByPk(user.id);
      res.send({ user });
    } catch (e) {
      Helper.sendError(res, e);
    }
  }
}
